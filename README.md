# Open-Elevation Instance Builder

[Open-Elevation](https://open-elevation.com/) is a free and open-source elevation API.

problems:
- Due to a lack of donations the public API is hosted on a server that can't handle the large amout of requests.
- The "host your own" instance is broken due to dead links.

solution:

Run this script to build a working Open-Elevation instance; it will download the raw data from alive links.

## Prerequisites

- Linux or OSX
- unrar
- docker (running)
- either a very good connexion and CPU or time and patience

## About the data

- origin: [Shuttle Radar Topography Mission](https://en.wikipedia.org/wiki/Shuttle_Radar_Topography_Mission)
- precision: 250m
- range: from 56°S to 60°N

## How to request the API?

[API documentation](https://github.com/Jorl17/open-elevation/blob/master/docs/api.md)