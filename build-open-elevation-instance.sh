#!/bin/bash

function check-commands {
	command unrar > /dev/null 2>&1
	if [[ $? -ne 0 ]]; then
		echo >&2 "unrar is not installed"
		exit 1
	fi
	command docker > /dev/null 2>&1
	if [[ $? -ne 0 ]]; then
		echo >&2 "docker is not installed"
		exit 1
	fi
	command docker info > /dev/null 2>&1
	if [[ $? -ne 0 ]]; then
		echo >&2 "docker deamon is not running or got permission denied while trying to access it"
		exit 1
	fi
}

function goto-data {
	if [[ ! -d data ]]; then
		mkdir data
	fi
	cd data || exit 1
}

function install-dataset {
	archive="srtm_$(echo $1 | tr '[:upper:]' '[:lower:]')_250m_tif.rar"
	goto-data \
	&& wget "http://srtm.csi.cgiar.org/wp-content/uploads/files/250m/${archive}" \
	&& unrar e "${archive}" \
	&& rm "${archive}" readme.txt \
	&& cd .. \
	&& echo "creating tiles..." \
	&& docker run -t -i -v "$(pwd)/data:/code/data" openelevation/open-elevation /code/create-tiles.sh "/code/data/SRTM_${1}_250m.tif" 10 10 \
	&& rm "data/SRTM_${1}_250m.tif"
}

function create-boot-script {
	echo 'docker run -t -i -v $(pwd)/data:/code/data -p 8080:8080 openelevation/open-elevation' > run-server.sh \
	&& chmod 750 run-server.sh
}

function main {
	check-commands \
	&& git clone http://github.com/Jorl17/open-elevation \
	&& cd open-elevation \
	&& install-dataset NE \
	&& install-dataset W \
	&& install-dataset SE \
	&& create-boot-script \
	&& echo 'Open-Elevation instance installed, launch "run-server.sh" to start the server'
}

main
